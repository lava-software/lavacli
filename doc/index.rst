.. _index:

lavacli
#######

lavacli is a command line tool to interact with one or many LAVA instances using XML-RPC.


Features
========

lavacli allows you to interact with all LAVA objects:

* aliases
* device-types
* devices
* events
* jobs
* results
* tags
* workers


Table of Contents
=================

.. toctree::
   :maxdepth: 2

   install
   configuration
   usage
